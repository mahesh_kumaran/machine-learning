import re 
import requests
import os
import time
import sys
import json
from pymongo import MongoClient
import ast
import argparse
import imutils
import numpy as np

import pytesseract

from PIL import Image
import cv2 as cv2

pytess_results = {
	"total_records": {
        "total": 0,
		"correct": 0,
		"incorrect": 0
	},
	"date": {   
		"correct": 0,
		"incorrect": 0
	},
	"month": {
		"correct": 0,
		"incorrect": 0
	},
	"year": {
		"correct": 0,
		"incorrect": 0
	},
	"pan_id": {
		"correct": 0,
		"incorrect": 0
	}
}

# Setup mongodb
client = MongoClient(connect=False,host="localhost:27017")
db = client.bigboss

                
def parse_pandata(raw_content):

    tesseract_record = {} 

    # parse 10 digit alphanumeric PAN number from text 
    pan_number = re.search(r"(?=[A-Z0-9]*[A-Z])(?=[A-Z0-9]*[0-9])[A-Z0-9]{10}", raw_content)
    if pan_number != None: 
        tesseract_record['pan_id'] = pan_number.group(0)
    else:
        tesseract_record['pan_id'] = ""


    # parse date of birth value from tesseract data 
    dob_match =  re.search(r"[0-9]+/[0-9]+/[0-9]+",raw_content)
    if dob_match != None : 
        dob = dob_match.group()
        tesseract_record['dob'] = {}
        tesseract_record['dob']['date'] = dob.split('/')[0]
        tesseract_record['dob']['month'] = dob.split('/')[1]
        tesseract_record['dob']['year'] = dob.split('/')[2]

    return tesseract_record


def match_records(pan_record, tesseract_record):
    
    record_flag = 0

    # print(pan_record)
    # print(tesseract_record)
    pytess_results['total_records']['total'] += 1
    try : 

        if tesseract_record != None:

            if 'pan_id' in tesseract_record.keys() and pan_record['pan_id'] == tesseract_record['pan_id']:
                pytess_results['pan_id']['correct'] += 1
            else: 
                pytess_results['pan_id']['incorrect'] += 1
                record_flag = 1

            if  'dob' in tesseract_record.keys(): 
                if pan_record['dob']['date']  == tesseract_record['dob']['date']:
                    pytess_results['date']['correct'] += 1
            else: 
                pytess_results['date']['incorrect'] += 1
                record_flag = 1

            
            if 'dob' in tesseract_record.keys():
                if pan_record['dob']['month']  == tesseract_record['dob']['month']:
                    pytess_results['month']['correct'] += 1
            else: 
                pytess_results['month']['incorrect'] += 1
                record_flag = 1

            if 'dob' in tesseract_record.keys():
                if pan_record['dob']['year']  == tesseract_record['dob']['year']:
                    pytess_results['year']['correct'] += 1
            else: 
                pytess_results['year']['incorrect'] += 1
                record_flag = 1
        else: 
            record_flag = 1

        if record_flag == 0:
            pytess_results['total_records']['correct'] += 1
        else:
            pytess_results['total_records']['incorrect'] += 1
    
        # print(pytess_results)
        # input()

    except Exception as e :

        # print('tesseract_record : ', tesseract_record, e)
        pass     


def parse_using_tesseract():
    
    pan_data = list(db.pan.find({}))

    print("Number of records to be parsed : ", len(pan_data))

    for index,each_record in enumerate(pan_data): 
        # print(each_record)
        try : 
            image_name = each_record['image_name']
            # print('./pan-images/' + image_name)
            
            # open an image using open-cv2
            img = cv2.imread('./pan-images/' + image_name, 0) # passing parameter loads the image in grey-scale 

            # save grey-scale image using open-cv2
            # cv2.imwrite('./preprocessed/' + image_name ,img)
            
            # pass the image to tesseract and get the text 
            text = pytesseract.image_to_string(img, lang='en')
            
            pytesseract_record = parse_pandata(text)
            match_records(each_record, pytesseract_record)
            # input()

            if index == len(pan_data) - 1: 
                print(pytess_results)
        except Exception as e :
            # print(e, image_name)
            pass



def enhance_image_brightness(file_path):

    import cv2 as cv2

    im = cv2.imread(file_path)

    # Check for orientation and rotate the image based on the orientation 

    orientation = pytesseract.image_to_osd(im, output_type=pytesseract.Output.DICT)
    if orientation['rotate'] != 180:
        im = np.rot90(im)

    # Increase the image dpi by default to all the images -- set to 300 DPI by default

    length_x, width_y, temp = im.shape
    factor = min(1, float(1024.0 / length_x))
    size = int(factor * length_x), int(factor * width_y)
    im_resized = im.resize(size, Image.ANTIALIAS)
    temp_filename = 'new_test.jpg'
    im_resized.save(temp_filename, dpi=(300, 300))


    # Noise Removal

    filtered = cv2.adaptiveThreshold(img.astype(np.uint8), 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 9, 41)
    kernel = np.ones((1, 1), np.uint8)
    opening = cv2.morphologyEx(filtered, cv2.MORPH_OPEN, kernel)
    closing = cv2.morphologyEx(opening, cv2.MORPH_CLOSE, kernel)
    img = image_smoothening(img)
    or_image = cv2.bitwise_or(img, closing)
    return or_image



enhance_image_brightness('./test_image.jpg')
# enhance_image_brightness('./ort.jpg')


































